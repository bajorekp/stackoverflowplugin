package pl.bajorekp.stackoverflowPlugin;

/**
 * Created with IntelliJ IDEA.
 * User: Tomek
 */
public class Main {
    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new MainJFrame(null);
            }
        });
    }
}
