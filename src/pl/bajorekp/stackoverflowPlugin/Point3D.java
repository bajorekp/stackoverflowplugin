package pl.bajorekp.stackoverflowPlugin;

/**
 * Created by bajorekp on 09/02/14.
 */
public class Point3D<T1, T2, T3> {

    public T1 x;
    public T2 y;
    public T3 z;

    Point3D (T1 x, T2 y, T3 z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
