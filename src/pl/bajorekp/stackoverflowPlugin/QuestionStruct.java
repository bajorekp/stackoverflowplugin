package pl.bajorekp.stackoverflowPlugin;

/**
 * Struct of searching by new post
 *
 * @author BajorekP
 */
public class QuestionStruct {

    // number of positive vote
    public int vote;
    // title of the post
    public String title;
    // Id of the post
    public int questionId;

    public QuestionStruct(int vote, String title, int questionId) {
        this.vote = vote;
        this.title = title;
        this.questionId = questionId;
    }

    public QuestionStruct() {
        this.vote = 0;
        this.title = "Unlocated";
        this.questionId = 0;
    }

    @Override
    public String toString() {
        return "{ vote: " + this.vote + ", title: " + this.title + ", questionId: " + this.questionId + " }";
    }
}
