package pl.bajorekp.stackoverflowPlugin;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Created by bajorekp on 17/03/14.
 */
public class AnswerCellJPanel extends JPanel {
    private RSyntaxTextArea codeTextArea;
    private JLabel voteLabel;

    public AnswerCellJPanel(int vote, String code) {
        this.createNewLayout();
        setVote(vote);
        setCode(code);
    }

    public AnswerCellJPanel() {
        this.createNewLayout();
        setVote(0);
        setCode("");
    }

    private void createNewLayout() {
        FlowLayout layout = new FlowLayout(FlowLayout.LEFT, 2, 3);
        setLayout(layout);

        voteLabel = new JLabel();

        // view for Vote Label
        voteLabel.setFont(new Font("Arial", Font.BOLD, 32));
        voteLabel.setForeground(new Color(126, 34, 24));
        add(voteLabel);

        codeTextArea = new RSyntaxTextArea();
        codeTextArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
        codeTextArea.setCodeFoldingEnabled(true);
        codeTextArea.setAntiAliasingEnabled(true);
        codeTextArea.setEditable(false);

        // view for code
        codeTextArea.setMinimumSize(new Dimension(300, 30));
        codeTextArea.setBackground(new Color(241, 225, 185));
        codeTextArea.setForeground(new Color(0, 0, 0));
        Border border = BorderFactory.createLineBorder(new Color(187, 112, 62),2,false);
        codeTextArea.setBorder(border);

        add(codeTextArea);
    }

    public void setVote(int vote) {
        if(voteLabel == null) {
            System.out.println("voteLabel wasn't set.");
            return;
        }
        voteLabel.setText("+" + vote);
    }

    public void setCode(String code) {

        if(codeTextArea == null) {
            System.out.println("codeTextArea wasn't set.");
            return;
        }
        codeTextArea.setText(code);
    }
}
