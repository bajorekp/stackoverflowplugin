package pl.bajorekp.stackoverflowPlugin;

import com.intellij.openapi.util.Pair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple class to manage stackoverflow posts.
 *
 * @author BajorekP, Mateusz Grabowski
 * @version 0.1
 */
public class StackoverflowAPI {
    private List<QuestionStruct> questionsList;
    private List<AnswerStruct> answersList;

    /**
     * Search function to find codes in stackoverflow site match to Query problem.
     *
     * @param Query Your problem which you want to resolve.
     * @return List with code which may help you.
     */
    public List<AnswerStruct> search(String Query) {

        questionsList = (ArrayList) findByQAlgorithm(Query);
        int maxVote = Integer.MIN_VALUE;
        int maxVoteID = 0;
        for (QuestionStruct question : questionsList) {
            question.vote = rateResult(Query, question.title);
            if (maxVote < question.vote) {
                maxVoteID = question.questionId;
                maxVote = question.vote;
            }
        }

        answersList = getBestAnswers(maxVoteID);

        return answersList;
    }

    /**
     * Function showing body of post founded by Id.
     *
     * @param postId Id of post which you want see.
     * @return Body of the post.
     * @throws NullPointerException
     */
    public String showPost(int postId) throws NullPointerException {
        String post = null;
        try {
            post = showAnswersListPost(postId);
        } catch (Exception e) {
            //e.printStackTrace();      //powinno pokazywac post z listy
            post = downloadPostBody(postId);
        }
        return post;
    }

    /**
     * Try find in answer written in answersList and if it will find return body of it.
     *
     * @param answerId Id of the answer
     * @return Body of answer
     * @throws NullPointerException
     */
    private String showAnswersListPost(int answerId) {
        for (AnswerStruct answer : answersList) {
            if (answer.answerId == answerId) {
                return answer.post;
            }
        }
        throw new NullPointerException();
    }

    /**
     * Download body of the post given by ID number.
     *
     * @param postId ID of the post.
     * @return Body of the post.
     */
    private String downloadPostBody(int postId) {
        try {
            // get whole post, which in fact include only body field
            JSONArray post = getItems("https://api.stackexchange.com/2.1/posts/" + postId +
                    "?order=desc&sort=activity&site=stackoverflow&filter=!3uhqFxylCStMJeq.iJ(5");
            // hask only body field
            return ((JSONObject) post.get(0)).get("body").toString();
        } catch (Exception e) {
            return null;
        }

    }

    /**
     * Get the best Answers of the question sort by votes.
     *
     * @param questionId ID number of the question
     * @return List of the answers
     */
    private List<AnswerStruct> getBestAnswers(int questionId) {
        String url = "https://api.stackexchange.com/2.1/questions/" + questionId +
                "/answers?order=desc&sort=votes&site=stackoverflow&filter=!9f8L7BVrc";
        JSONArray answers = getItems(url);
        HtmlBrowser htmlBrowser = new HtmlBrowser();


        List<AnswerStruct> answersList = new ArrayList<AnswerStruct>();

        for (Object answer : answers) {
            JSONObject post = (JSONObject) answer;
            int vote = Integer.parseInt(post.get("score").toString());
            String body = post.get("body").toString();
            int answerId = Integer.parseInt(post.get("answer_id").toString());
            ArrayList<Pair<Integer,Integer>> codes = getPureCode(body);
            answersList.add(new AnswerStruct(vote, body, answerId, questionId, codes));
        }
        return answersList;
    }

    /**
     * Function return codes sections from post body in Pairs Array with beginning and ending of fragment of code.
     * @param body Body of the post
     * @return Array of codes in post
     */
    public ArrayList<Pair<Integer,Integer>> getPureCode(String body) {
        int lastCursorPosition = 0;
        ArrayList<Pair<Integer,Integer>> codes = new ArrayList<Pair<Integer, Integer>>();
        while (lastCursorPosition != -1) {
            lastCursorPosition = body.indexOf("<pre><code>", lastCursorPosition);
            if (lastCursorPosition != -1) {
                int start = lastCursorPosition;
                lastCursorPosition = body.indexOf("</code></pre>", lastCursorPosition);
                if (lastCursorPosition != -1) {
                    codes.add(new Pair<Integer, Integer>(start + 11, lastCursorPosition));
                }
            }
        }

        return codes;
    }

    /**
     * Find the similar question to yours by searching titles
     *
     * @param inTitle text which will be compare with title
     * @return List of the Question
     */
    public List<QuestionStruct> searchInTitle(String inTitle) {
        JSONArray items = this.getItems("https://api.stackexchange.com/2.1/search?order=desc&sort=votes" +
                "&intitle=" + inTitle +
                "&site=stackoverflow&filter=!LJBGVkK3)oq0fNrhl0dpkVJsB");
        System.out.println(items.toString());

        questionsList = new ArrayList<QuestionStruct>();
        for (Object object : items) {
            JSONObject question = (JSONObject) object;
            if (question.get("is_answered").toString().equals("true")) {
                int id = Integer.parseInt(question.get("question_id").toString());
                int vote = Integer.parseInt(question.get("score").toString());
                String title = question.get("title").toString();
                questionsList.add(new QuestionStruct(vote, title, id));
            }
            System.out.println(questionsList);
        }
        return questionsList;
    }

    /**
     * Q is undocumented algorithm which is used to search Stackoverflow site.
     * Function is used to find questions by this algorithm
     *
     * @param Query Your problem which you want to resolve.
     * @return Return list of the question which are similar to Query.
     */
    private List<QuestionStruct> findByQAlgorithm(String Query) {
        JSONArray questions = getItems("https://api.stackexchange.com/2.1/search/advanced?order=desc&sort=votes&q=" +
                Query + "&site=stackoverflow&filter=!)qQ8TpJRGA_Jc7fWeeiafE.");

        List<QuestionStruct> questionList = new ArrayList<QuestionStruct>();
        for (Object obj : questions) {
            JSONObject question = (JSONObject) obj;
            int vote = Integer.parseInt(question.get("score").toString());
            String title = question.get("title").toString();
            int questionId = Integer.parseInt(question.get("question_id").toString());
            QuestionStruct questionStruct = new QuestionStruct(vote, title, questionId);
            questionList.add(questionStruct);
        }
        return questionList;
    }

    private int rateResult(String Query, String Title) {
        String[] titleArray = Title.split(" ");
        String[] queryArray = Query.split(" ");
        int score = 0;
        for (String query : queryArray) {
            boolean founded = false;
            for (String title : titleArray) {
                if (query.equalsIgnoreCase(title)) {
                    score++;
                    founded = true;
                }
            }
            if (!founded) {
                score--;
            }
        }
        return score;
    }

    /**
     * Get Items JSONArray from stackoverflow site.
     *
     * @param stackUrl Whole url
     * @return JSONArray of JSONObjects in Items Array
     * @throws IllegalArgumentException If can't be converted right url.
     */
    private JSONArray getItems(String stackUrl) {
        String stackPage = null;
        String stringUrl = null;
        HtmlBrowser htmlBrowser = new HtmlBrowser();

        try {

            // check if it's good url for stackoverflow api in 2.1 version
            if (!stackUrl.startsWith("https://api.stackexchange.com/2.1")) {
                throw new UnsatisfiedLinkError();
            }

            int locationStart = stackUrl.indexOf("/2.1/");
            int dataStart = stackUrl.indexOf("?");

            stringUrl = (new URI("https", "api.stackexchange.com",
                    stackUrl.substring(locationStart, dataStart),
                    stackUrl.substring(dataStart + 1, stackUrl.length()) + "&key=dc6tAToE)tsoCXszSIyYQw((",
                    "")).toString();


        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        stackPage = htmlBrowser.download(stringUrl);

        JSONParser praser = new JSONParser();
        JSONObject object = null;

        try {
            object = (JSONObject) praser.parse(stackPage);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        JSONArray items = (JSONArray) object.get("items");   // (JSONArray) rzutowanie na JSONArray!!!

        return items;

    }
}