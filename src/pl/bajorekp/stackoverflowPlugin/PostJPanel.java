package pl.bajorekp.stackoverflowPlugin;

import com.intellij.ui.components.JBScrollPane;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tomek
 */
public class PostJPanel extends JPanel {

    MainJFrame mainFrame;
    JEditorPane editorPane;
    JScrollPane jScrollPane;

    public PostJPanel(final MainJFrame mainFrame){
     super();
     this.mainFrame = mainFrame;
     this.setPreferredSize(new Dimension(300, 300));
     this.setBorder(BorderFactory.createLineBorder(Color.RED, 2, true));
     this.setBackground(Color.ORANGE);


        //Editor Panel which give ability to check and copy text from the post
        editorPane = new JEditorPane();
        editorPane.setEditable(false);
        editorPane.setContentType("text/html");
        editorPane.setPreferredSize(new Dimension(290, 290));
        editorPane.setBackground(Color.ORANGE);
        editorPane.setText("");

        jScrollPane = new JBScrollPane(editorPane);
        add(jScrollPane);

        this.setVisible(false);
    }

    public void changeText(String text){
        editorPane.setText("<html><div style=\"margin: 5px;\">" + text + "</div></html>");
        editorPane.revalidate();
        editorPane.setCaretPosition(0);
    }
}
