package pl.bajorekp.stackoverflowPlugin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: tomekkrzyszko
 */

public class QuestionJPanel extends JPanel implements ActionListener {

    MainJFrame mainFrame;
    JButton searchButton;
    static JTextField textField;

    public QuestionJPanel(final MainJFrame mainFrame){
        super();

        //UIManager
        UIManager.put("Button.background",Color.WHITE);

        setBackground(Color.WHITE);
        setLayout(new BorderLayout());
        this.mainFrame = mainFrame;
        this.setPreferredSize(new Dimension(500,30));
        this.setSize(new Dimension(500,30));
        this.setBorder(BorderFactory.createLineBorder(Color.black));

        //Button for searching
        ImageIcon searchButtonicon = createImageIcon("lupa.png");
        searchButton = new JButton (searchButtonicon);
        //searchButton.setBackground(Color.WHITE);
        searchButton.addActionListener(this);
        searchButton.setToolTipText("Click this button if you want to search.");
        searchButton.setBorder(BorderFactory.createEmptyBorder(2,0,2,4));

        //TextField for asking questions
        textField = new JTextField(25);
        textField.addActionListener(this);
        textField.setActionCommand("clear");
        textField.setToolTipText("Ask Your question.");
        textField.setBorder(BorderFactory.createEmptyBorder(2,4,2,0));

        // create JPanel as container for searchButton and textField
        JPanel searchPanel = new JPanel(new BorderLayout());
        searchPanel.add(textField, BorderLayout.WEST);
        searchPanel.add(searchButton, BorderLayout.EAST);
        searchPanel.setBackground(Color.WHITE);
        searchPanel.setBorder(BorderFactory.createLineBorder(Color.ORANGE, 1, true));
        searchPanel.setPreferredSize(new Dimension(200, 30));

        //Inscription
        ImageIcon stackoverflowIcon = createImageIcon("stackoverflow_icon.png");
        JLabel stackLabel = new JLabel(stackoverflowIcon);
        stackLabel.setSize(new Dimension(300, 30));


        //Adding components
        add(stackLabel, BorderLayout.LINE_START);
        add(searchPanel, BorderLayout.CENTER);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        mainFrame.setPanel2Visible();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                mainFrame.listPanel.szukaj(textField.getText());
            }
        });
    }



    protected ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
}
