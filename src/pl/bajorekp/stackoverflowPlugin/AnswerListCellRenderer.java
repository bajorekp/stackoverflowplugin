package pl.bajorekp.stackoverflowPlugin;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: tomekrzyszko
 */
public class AnswerListCellRenderer extends JLabel implements ListCellRenderer {

    public AnswerListCellRenderer() {
        setOpaque(true);
    }

    public Component getListCellRendererComponent(JList list, Object value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {
        //General settings of JList
        setText(value.toString());
        setBorder(BorderFactory.createRaisedBevelBorder());

        if (isSelected) {
            //Settings when cell is selected
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());

        } else {
            //Setting when cell is not selected
            setBackground(Color.ORANGE);
            setForeground(list.getForeground());
        }
        return this;
    }

}
