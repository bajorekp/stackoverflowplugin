package pl.bajorekp.stackoverflowPlugin;

import com.intellij.openapi.util.Pair;

import java.util.ArrayList;

/**
 * Struct of answer in stackoverflowPlugin
 *
 * @author BajorekP
 */

public class AnswerStruct {
    //number of votes for
    public int vote;
    //post in html
    public String post;
    //id number of the answer
    public int answerId;
    //id number of main post which is for the answer
    public int questionId;
    //ArrayList of codes section
    public ArrayList<Pair<Integer,Integer>> codes;

    public AnswerStruct(int vote, String post, int answerId, int questionId, ArrayList codes) {
        this.vote = vote;
        this.answerId = answerId;
        this.questionId = questionId;
        this.post = post;
        this.codes = codes;
    }

    public AnswerStruct() {
        this.vote = 0;
        this.answerId = 0;
        this.questionId = 0;
        this.post = "Unallocated";
        this.codes = null;
    }

    @Override
    public String toString() {
        return "{ vote: " + this.vote + ", questionId: " + this.questionId + ", answerId: " + answerId + ", post: " + this.post + ", codes:" + codes.toString() + " }";
    }
}
