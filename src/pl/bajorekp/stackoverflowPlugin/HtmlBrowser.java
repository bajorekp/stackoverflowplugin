package pl.bajorekp.stackoverflowPlugin;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

/**
 * Simple class to download web pages including coded sites.
 *
 * @author BajorekP
 */
public class HtmlBrowser {

    /**
     * Download function to download html page coded in GZip and Deflate
     *
     * @param urlString String which should look like this: http://domain.com/etc
     * @return String containing web page source
     */
    public String download(String urlString) {
        URL url = null;
        InputStream inStr = null;
        StringBuffer buffer = new StringBuffer();

        try {
            // make new url
            url = new URL(urlString);
            // Cast shouldn't fail
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            HttpURLConnection.setFollowRedirects(true);
            // allow both GZip and Deflate (ZLib) encodings
            conn.setRequestProperty("Accept-Encoding", "*");
            String encoding = conn.getContentEncoding();
            inStr = null;

            // create the appropriate stream wrapper based on
            // the encoding type
            if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
                inStr = new GZIPInputStream(conn.getInputStream());
            } else if (encoding != null && encoding.equalsIgnoreCase("deflate")) {
                inStr = new InflaterInputStream(conn.getInputStream(),
                        new Inflater(true));
            } else {
                inStr = conn.getInputStream();
            }
            int ptr = 0;

            InputStreamReader inStrReader = new InputStreamReader(inStr, Charset.forName("GB2312"));

            while ((ptr = inStrReader.read()) != -1) {
                buffer.append((char) ptr);
            }
            inStrReader.close();

            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inStr != null)
                try {
                    inStr.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }

        return buffer.toString();
    }
}
