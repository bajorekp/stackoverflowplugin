package pl.bajorekp.stackoverflowPlugin;

import com.intellij.openapi.actionSystem.AnActionEvent;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tomek
 */
public class MainJFrame extends JFrame {

    private LayoutManager layoutManager;
    public QuestionJPanel questionPanel;
    ListJPanel listPanel;
    public AnActionEvent mainAction;
    public PostJPanel postJPanel;

    public MainJFrame(AnActionEvent action) throws HeadlessException {
        super();
        mainAction = action;

        layoutManager = new GridBagLayout();
        this.setLayout(layoutManager);
        this.setUndecorated(true);
        this.setBackground(new Color(1,1,1,0));

        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((int)(screenDimension.getWidth()/4),(int)screenDimension.getHeight()/4);

        GridBagConstraints constraints = new GridBagConstraints();

        questionPanel = new QuestionJPanel(this);
        listPanel = new ListJPanel(this);
        postJPanel = new PostJPanel(this);


        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.anchor = GridBagConstraints.FIRST_LINE_START;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1;
        constraints.gridwidth = 2;
        add(questionPanel, constraints);

        constraints.fill = GridBagConstraints.VERTICAL;
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.weighty = 1;
        constraints.weightx = 0.5;
        constraints.gridwidth = 1;
        add(listPanel, constraints);

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.anchor = GridBagConstraints.FIRST_LINE_END;
        constraints.gridx = 2;
        constraints.gridy = 1;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.gridheight = GridBagConstraints.REMAINDER;
        constraints.ipady = 40;
        add(postJPanel,constraints);

        listPanel.setVisible(false);
        postJPanel.setVisible(false);

        this.pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }


    public void setPanel2Visible()
    {
        listPanel.setVisible(true);
        this.pack();
    }

    public void setpostPanelVisible()
    {
        postJPanel.setVisible(true);
        this.pack();
    }


    @Override
    public void pack() {
        super.pack();
        this.repaint();

    }

}
