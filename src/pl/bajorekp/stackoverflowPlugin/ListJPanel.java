package pl.bajorekp.stackoverflowPlugin;

import com.intellij.openapi.util.Pair;
import com.intellij.ui.components.JBList;
import com.intellij.ui.components.JBScrollPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: tomekkrzyszko
 */

public class ListJPanel extends JPanel {

    public DefaultListModel listModel;
    public JList list;
    public java.util.List<AnswerStruct> answers;
    public ArrayList<Point3D<Integer,Integer,Integer>> codes;

    public ListJPanel(final MainJFrame mainFrame){

        super(new BorderLayout());
        this.setPreferredSize(new Dimension(430, 500));
        this.setSize(new Dimension(430, 500));
        this.setBackground(Color.WHITE);

        //Panel with code area
        JPanel listCellPanel= new JPanel();
        JTextArea codeTextArea = new JTextArea();
        listCellPanel.add(codeTextArea);


        listModel = new DefaultListModel();

        //Answer JList
        list = new JBList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);
        list.setSelectedIndex(0);
        list.setVisibleRowCount(-1);
        list.getInputMap().put(KeyStroke.getKeyStroke("ENTER"),
                "showDialog");
        list.getActionMap().put("showDialog",
                new AbstractAction(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        System.out.println("Przetwarzam post...");
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("List selection #" + list.getSelectedIndex());
                        mainFrame.setpostPanelVisible();
                        try
                        {
                            stringBuilder.append(answers.get(codes.get(list.getSelectedIndex()).x).post);
                        } finally {
                          mainFrame.postJPanel.changeText(stringBuilder.toString());
                        }
                    }
                });
        list.getInputMap().put(KeyStroke.getKeyStroke("SPACE"),
                "putCode");
        list.getActionMap().put("putCode",
                new AbstractAction(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        StackoverflowAPI s = new StackoverflowAPI();
                        int i = codes.get(list.getSelectedIndex()).x;
                        int start = codes.get(list.getSelectedIndex()).y;
                        int end = codes.get(list.getSelectedIndex()).z;
                        String code = answers.get(i).post.substring(start,end);
                        PutCodeToEditor putCodeToEditor = new PutCodeToEditor(mainFrame.mainAction,code);
                    }
                });
        list.setCellRenderer(new AnswerListCellRenderer());

        //Scroll control
        JScrollPane listScrollPane = new JBScrollPane(list);
        add(listScrollPane);
    }

    public void szukaj(String question) {

                StackoverflowAPI stackoverflowAPI = new StackoverflowAPI();
                listModel.clear();
                answers = stackoverflowAPI.search(question);
                codes = new ArrayList<Point3D<Integer, Integer, Integer>>();
                if(answers == null || answers.size() == 0)
                {
                    listModel.addElement("No results is found.");
                } else
                {
                    System.out.println("Szukam...");
                    int PostIndex = 0;
                    for(AnswerStruct answer : answers)
                    {
                        try
                        {
                            for(Pair<Integer,Integer> code : answer.codes)
                            {
                                codes.add(new Point3D<Integer, Integer, Integer>(PostIndex,code.first,code.second));
                                listModel.addElement("<html><table><tr><td>"+"+"+
                                                     answer.vote+"</td><td>" +"<pre><code>" +
                                                     answer.post.substring(code.first,code.second) + "</code></pre></td></tr></table></html>");
                            }
                        } catch (Exception ex)
                        {

                        }
                        PostIndex++;
                    }
                }
                list.setSelectedIndex(0);
                list.requestFocusInWindow();

    }


}
