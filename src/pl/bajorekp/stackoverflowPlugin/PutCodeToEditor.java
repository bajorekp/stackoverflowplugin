package pl.bajorekp.stackoverflowPlugin;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.PsiDocumentManagerImpl;
import com.intellij.psi.util.PsiTreeUtil;

import java.awt.event.ActionEvent;

/**
 * Created by bajorekp on 08/02/14.
 */
public class PutCodeToEditor {
    public PutCodeToEditor(final AnActionEvent e, final String code) {
        // rozpoczęcie akcji zapisywania do edytora
        ApplicationManager.getApplication().runWriteAction(new Runnable() {
            @Override
            public void run() {
                PsiFile psiFile = e.getData(LangDataKeys.PSI_FILE);
                Editor editor = e.getData(LangDataKeys.EDITOR);
                Project project = e.getData(LangDataKeys.PROJECT);

                if(psiFile == null || editor == null)
                {
                    e.getPresentation().setEnabled(false);
                    return;
                }
                int offset = editor.getCaretModel().getOffset();
                PsiElement elementAt = psiFile.findElementAt(offset);
                PsiClass psiClass = PsiTreeUtil.getParentOfType(elementAt, PsiClass.class);
                PsiDocumentManager.getInstance(project).getDocument(psiFile).insertString(offset, code);
            }
        });
    }
}
