DEVELOPERS

Configuration

Your configuration file (*.iml) should have line which set project as plugin module


            <module type="PLUGIN_MODULE" version="4">

and add plugin.xml file to project to provide running action


            <component name="DevKit.ModuleBuildProperties" url="file://$MODULE_DIR$/META-INF/plugin.xml" />

Whole file should look like this one:


            <?xml version="1.0" encoding="UTF-8"?>
            <module type="PLUGIN_MODULE" version="4">
                <component name="DevKit.ModuleBuildProperties" url="file://$MODULE_DIR$/META-INF/plugin.xml" />
                <component name="NewModuleRootManager" inherit-compiler-output="true">
                    <exclude-output />
                    <content url="file://$MODULE_DIR$">
                        <sourceFolder url="file://$MODULE_DIR$/src" isTestSource="false" />
                    </content>
                    <orderEntry type="inheritedJdk" />
                    <orderEntry type="sourceFolder" forTests="false" />
                    <orderEntry type="library" name="json-simple-1.1" level="project" />
                </component>
            </module>


